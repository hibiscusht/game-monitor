import home from "./home.js" 
import register from "./register.js"
import dashboard from "./dashboard.js"
import menu from "./menu.js"
import env from "./env.js"
import global from "./global.js"
import watchboard from "./watchboard.js"

const routes = [ 
    {
        url: '/',
        target: ()=>{
               return home.tes()
        }
    },
    {
        url: '/register',
        target: ()=>{
               return register.resolve()
        }
    },
    {
        url: '/menu',
        target: ()=>{
               return menu.resolve()
        }
    },
    {
        url: '/env',
        target: ()=>{
               return env.resolve()
        }
    },
    {
        url: '/dashboard',
        target: ()=>{
               return dashboard.resolve()
        }
    },
    {
        url: '/expired',
        target: ()=>{
               return global.reload()
        }
    },{
        url: '/watchboard',
        target: ()=>{
               return watchboard.resolve()
        }
    }
]

export { routes }