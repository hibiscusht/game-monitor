
class Watchboard {
 

    resolve(page){  

        loadPlayData()
       
        return `<!-- Product section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <div class="col-md-10"> DATA MAXWIN RUN & CRASH
                    <hr/>
                     <button onclick="loadPlayData()"> load all data </button> 

                    <hr/>
                    <table class="table table-striped table-bordered">
        <thead>
             <tr>
                <th>Periode</th>
                <th>Waktu</th>
                <th>Jenis</th>
                <th>Angka</th>
                <th>Mode</th> 
             </tr>
        </thead>
        <tbody id="data"></tbody>
        <tfoot>
            <tr>
              
                <td><button onclick="loadOtherPlayData('before')"> prev </button></td>
                <td><button onclick="loadOtherPlayData('after')"> next </button></td>
            </tr>
        </tfoot>
    </table>
                    <div class="col-md-5"> 
                   
                    </div>
                    </div>
                    </div>    
                </div>
            </div>
        </section>
        <!-- Related items section-->
            
        `
    }

}

const watchboard = new Watchboard()
export default watchboard