
async function loadme(name){
   route(name)
} 

async function register(){
   const form = document.querySelectorAll('.regist')
   let fd = {}
   for(const elem of form){
      if(elem.dataset.key == 'password'){
         fd[`${elem.dataset.key}`] = btoa(elem.value)
      }else {
       fd[`${elem.dataset.key}`] = elem.value
        }
   }
  
   const hdr = new Headers
   hdr.append('Content-Type','application/json')
   hdr.append('Authorization','Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2')
   const data = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/users/data',{
      method: 'POST',
      headers: hdr,
      body: JSON.stringify(fd)
   })
   const result = await data.json()
   if(data.ok){
      alert('Registrasi Berhasil')
   }
}


async function login(){
   const form = document.querySelectorAll('.login')
   const username = form[0].value
   const password = form[1].value
   const options = {
      method: 'POST',
      headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
      body: '{ "filter":{"username":{"$contains":"' + username + '"}} }'
    } 
   const data = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/users/query',options)
   const result = await data.json()
   if(data.ok){
       for(const row of result.records){
          if(btoa(password) == row.password){
            // simpan token
            const token = btoa((new Date()).toISOString())
            const options = {
               method: 'PATCH',
               headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
               body: '{"token":"'+ token +'"}'
             };
             
             const user = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/users/data/rec_clsod2h0ofb4j806nkcg?columns=id', options)

             
            // simpan token

             if(user.ok){
               localStorage.setItem('wingo_token',token)
               route('menu')
             }
              
             
          } else {
             alert('whoops wrong password')
          }
       }
   }
}


let cursor = ''
async function loadAfter(){ 
    await loadData('after',1)
}
async function loadBefore(){ 
    await loadData('before',0)
}
async function loadData(type,body){ 
    let body_data = '{"columns":["id","periode","waktu","jenis","nilai","tipe","bet","saldo"],"page":{"size":15},"sort":{"waktu":"desc"}}'
    if(body == undefined){
       if(type == 'win'){
         body_data = '{"columns":["id","periode","waktu","jenis","nilai","tipe","bet","saldo"],"filter":{"tipe":{"$contains":"WIN"}},"page":{"size":15},"sort":{"waktu":"desc"}}'
       }
    } else {
       body_data = '{"page":{"' + type + '":"' + cursor + '"}}'  
    } 

    const options = {
         method: 'POST',
         headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
         body: body_data
    }

    const data = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/record_bet/query', options) 
    const result = await data.json()

    if(data.ok){
        let body = ''
        for(const item of result.records){
            body += `<tr>`
            body += `<td>${item.periode}</td>`    
            body += `<td>${new Date(item.waktu).toLocaleString('id-Id')}</td>`  
            body += `<td>${item.jenis}</td>`
            body += `<td>${item.nilai}</td>` 
            body += `<td>${item.tipe}</td>` 
            body += `<td>Rp ${item.bet}</td>` 
            body += `<td>Rp ${item.saldo}</td>` 
            body += `</tr>`
        }
      document.querySelector('#data').innerHTML = body
      cursor = result.meta.page.cursor
    }
}

async function loadOtherPlayData(dir){
   await loadPlayData(dir,1)
}


async function loadPlayData(type,body){ 
   let body_data = '{"columns":["id","periode","waktu","jenis","nilai","mode"],"page":{"size":15},"sort":{"waktu":"desc"}}'
   if(body == undefined){
      if(type == 'win'){
        body_data = '{"columns":["id","periode","waktu","jenis","nilai","tipe","bet","saldo"],"filter":{"tipe":{"$contains":"WIN"}},"page":{"size":15},"sort":{"waktu":"desc"}}'
      }
   } else {
      body_data = '{"page":{"' + type + '":"' + cursor + '"}}'  
   } 

   const options = {
        method: 'POST',
        headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
        body: body_data
   }

   const data = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/playing_record/query', options) 
   const result = await data.json()

   if(data.ok){
       let body = ''
       for(const item of result.records){
           body += `<tr>`
           body += `<td>${item.periode}</td>`    
           body += `<td>${new Date(item.waktu).toLocaleString('id-Id')}</td>`  
           body += `<td>${item.jenis}</td>`
           body += `<td>${item.nilai}</td>` 
           body += `<td>${item.mode}</td>`  
           body += `</tr>`
       }
     document.querySelector('#data').innerHTML = body
     cursor = result.meta.page.cursor
   }
}

async function logout(){
   const options = {
      method: 'PATCH',
      headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
      body: '{"token":""}'
    };
    
    const user = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/users/data/rec_clsod2h0ofb4j806nkcg?columns=id', options)
   localStorage.removeItem('wingo_token')
   loadme('home')
}

async function kickout(){
   const token = localStorage.getItem('wingo_token')
   if(token == undefined || token == ''){
      
   if(history.state.page != ''){
       const router = document.querySelector('#router')
       router.setAttribute('onclick',`window.route('home')`)
       router.click()
       location.reload()
   } 

   } else {
      // expired kan token dalam 6 jam
      const token = atob(localStorage.getItem('wingo_token'))
      const before = (new Date(token)).getTime()
      const now = Date.now()
      const jeda = 5 * 60 * 1000
      if(now - before > jeda){
         console.log(now - before,jeda)
         localStorage.removeItem('wingo_token')
         const router = document.querySelector('#router')
         router.setAttribute('onclick',`window.route('home')`)
         router.click()
         location.reload()
      }
   }
}

async function get_env(){
   setTimeout(async ()=>{
      const options = {
         method: 'POST',
         headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
         body: '{"columns":["id","env_name","env_value"],"page":{"size":15}}'
       }; 
       
       const env = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/game_env/query', options)
       const env_data = await env.json()
   
       if(env.ok){
         const element = document.querySelectorAll('.input')  
         for(const elem of element){
           for(const data of env_data.records){ 
            if(elem.dataset.key == data.env_name){
               elem.setAttribute('data-id',data.id)
               elem.value = data.env_value
            }
           }

         }
       }
   },1500)
  
}

async function save_env(){
 
     
   const elems = document.querySelectorAll('.input')

   let cnt = 0
   
   for(const elem of elems){
     
      const options = {
         method: 'PATCH',
         headers: {Authorization: 'Bearer xau_0IHjue5w9tgDWnTD0Lv7Lc700fRSxDag2', 'Content-Type': 'application/json'},
         body: '{"env_value":"'+ elem.value +'"}'
       };
       
      const dt = await fetch('https://astomo-pancoro-putro-s-workspace-n629bs.ap-southeast-2.xata.sh/db/sct-pos:main/tables/game_env/data/'+ elem.dataset.id +'?columns=id', options)

      if(dt.ok){
         cnt++
      }

   }

  
    
   if(cnt > 0){
      alert('data berhasil disimpan')
      route('dashboard')
   }
  
}
  