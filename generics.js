//import data from "./data.js"

class Generics {

    loadPage(page){

        let elem = {}

        for(let item of data){
            if(item.gambar == page){
                elem = item
                break
            }
        }
        return elem
    }

    loadMore(json){

        let more = ''

        let items = []

        for(let j = 0; j < 4; j++){

            const rand = Math.floor(Math.random() * json.length)  
             
            items.push(rand) 

        }
 

        for(let n = 0; n < items.length; n++){

            const i = items[n]

            const harga = (json[i].harga_diskon != 0)? `<span class="text-muted text-decoration-line-through">Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga))}</span> Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga_diskon))}` : `Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga))}`

            more +=  `

            <div class="col mb-5">
                <div class="card h-100">
                    <!-- Product image-->
                    <img class="card-img-top" src="assets/img/${json[i].gambar}.png" alt="..." style="height: 30%"/>
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder">${json[i].nama}</h5>
                            <!-- Product price-->
                            ${harga}
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><p class="btn btn-outline-dark mt-auto" onclick="route('${json[i].gambar}')">View options</p></div>
                    </div>
                </div>
            </div> `

         

    }

        return more
    }

    resolve(page){ 
        const elem = this.loadPage(page)

        

        return `<!-- Product section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <div class="col-md-10"> </div>
                    
                </div>
            </div>
        </section>
        <!-- Related items section-->
            
        `
    }

}

const generics = new Generics()
export default generics